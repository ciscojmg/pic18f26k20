Requerimientos
===============
- [Proteus 8.8](https://www.labcenter.com/)
- [XC8 Compilador](https://www.microchip.com/development-tools/pic-and-dspic-downloads-archive)

Instalacion de XC8 en Proteus
=================================
- Presionar el boton `Source Code`


![2020-08-14_15h01_20](/uploads/0c0e40de35f273277dfecef2b1873568/2020-08-14_15h01_20.png)
- `Source Code >> System >> Compilers Configurations`
  - Seleccionar el Compilador **MPLAB XC8**
  - Presionar el boton **Manual**
  - Buscar la ruta donde esta instalado el compilador
    - Si estas en Windows normalmente se instala en:
      - `C:\Program Files\Microchip\xc8\v2.20`
  - Presionar el boton **Ok**

![2020-08-14_15h04_00](/uploads/25335d6c6b7fc8d5eb0a962bbb98248a/2020-08-14_15h04_00.png)